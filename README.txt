COMMUNITY DIRECTORY INSTALLATION PROFILE
========================================

The community directory installation profile is designed to present a directory
of communities, local centers, and national or regional facilities and schools.
Though it was designed specifically for Baha'i communities, it might be useful
for other organizations as well.


INSTALLATION
============

Follow the directions at http://drupal.org/node/306267.

Once the site is running, you may enable modular functionality at 
Structure -> Features in the admin menu.

