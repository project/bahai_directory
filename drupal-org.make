api = 2
core = 7.65


; Administrative and
projects[ctools] = 1.13
projects[token] = 1.7
projects[pathauto] = 1.3
projects[transliteration] = 3.2
projects[redirect][version] = 1.0-rc3
projects[admin_menu] = 3.0-rc5
projects[masquerade] = 1.0-rc7
projects[google_analytics] = 2.4
projects[custom_breadcrumbs] = 2.0-beta1
projects[libraries] = 2.3
projects[markdown] = 1.5
projects[smart_ip] = 2.50
projects[crumbs] = 2.6
projects[entity] = 1.9
projects[backup_migrate] = 3.5

; Views
projects[views] = 3.22
projects[views_data_export] = 3.2

; Fields
projects[field_group] = 1.6
projects[link] = 1.6
projects[link_options] = 1.0-alpha2
projects[references] = 2.2
projects[date] = 2.10

; Location and Maps
projects[location] = 3.7
projects[openlayers] = 2.0-beta11
projects[geophp] = 1.7
projects[proj4js] = 1.2

; Caching
projects[boost] = 1.2
projects[cacheexclude] = 2.3
projects[advagg] = 2.33
projects[expire] = 2.0-rc4
projects[ajaxblocks] = 1.4
projects[ajaxblocks][patch][1348918] = "https://www.drupal.org/files/issues/ajaxblocks-fix-boost-integration-1348918-16-D7_0.patch"
projects[esi][download][type] = git
projects[esi][download][branch] = 7.x-3.x
projects[esi][download][revision] = 58c076108754c877e5fae1b3900c2f624438bad8

; Features
projects[features] = 2.10
projects[features_override] = 2.0-rc3
projects[strongarm] = 2.0
projects[diff] = 3.3

; Import from CSV files
projects[job_scheduler] = 2.0-alpha3
projects[feeds] = 2.0-beta4
projects[location_feeds] = 1.6

; Fonts
projects[fontyourface] = 2.8

; Libraries
libraries[openlayers][download][type] = "file"
libraries[openlayers][download][url] = "https://github.com/openlayers/ol2/releases/download/release-2.13.1/OpenLayers-2.13.1.tar.gz"
;libraries[openlayers][patch][2353829] = "https://www.drupal.org/files/issues/remove-demos-from-openlayers-2-13-1-fixed.patch"
libraries[openlayers][patch][1442460] = "https://www.drupal.org/files/issues/1442460-remove-Rico-repo-for-gpl-compatibility.patch"
libraries[openlayers][directory_name] = "openlayers"
