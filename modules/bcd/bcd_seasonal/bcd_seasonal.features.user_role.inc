<?php
/**
 * @file
 * bcd_seasonal.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function bcd_seasonal_user_default_roles() {
  $roles = array();

  // Exported role: seasonal schools editor.
  $roles['seasonal schools editor'] = array(
    'name' => 'seasonal schools editor',
    'weight' => 4,
  );

  return $roles;
}
