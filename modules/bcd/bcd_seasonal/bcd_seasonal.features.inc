<?php
/**
 * @file
 * bcd_seasonal.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bcd_seasonal_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function bcd_seasonal_node_info() {
  $items = array(
    'seasonal' => array(
      'name' => t('Seasonal School'),
      'base' => 'node_content',
      'description' => t('A seasonal school of short duration possibly held at rented facilities.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
