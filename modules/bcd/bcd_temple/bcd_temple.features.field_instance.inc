<?php
/**
 * @file
 * bcd_temple.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function bcd_temple_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-temple-body'.
  $field_instances['node-temple-body'] = array(
    'bundle' => 'temple',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'full' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 500,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'required' => FALSE,
    'settings' => array(
      'display_summary' => TRUE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 31,
    ),
  );

  // Exported field_instance: 'node-temple-field_website'.
  $field_instances['node-temple-field_website'] = array(
    'bundle' => 'temple',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'link_options',
        'settings' => array(
          'link_formatter' => 'link_default',
          'really_redirect' => 0,
          'strip_www' => FALSE,
        ),
        'type' => 'link_options_default',
        'weight' => 1,
      ),
      'full' => array(
        'label' => 'inline',
        'module' => 'link_options',
        'settings' => array(
          'link_formatter' => 'link_default',
          'really_redirect' => 1,
          'strip_www' => 0,
        ),
        'type' => 'link_options_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'inline',
        'module' => 'link_options',
        'settings' => array(
          'link_formatter' => 'link_default',
          'really_redirect' => 0,
          'strip_www' => FALSE,
        ),
        'type' => 'link_options_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_website',
    'label' => 'Website',
    'required' => 0,
    'settings' => array(
      'absolute_url' => 1,
      'attributes' => array(
        'class' => '',
        'configurable_title' => 0,
        'link_options' => 'Displayed',
        'rel' => '',
        'target' => 'default',
        'title' => '',
        'use_link_options' => 1,
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'rel_remove' => 'default',
      'title' => 'optional',
      'title_label_use_field_label' => FALSE,
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => 33,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Body');
  t('Website');

  return $field_instances;
}
