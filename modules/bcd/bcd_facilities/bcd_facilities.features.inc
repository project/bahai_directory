<?php
/**
 * @file
 * bcd_facilities.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bcd_facilities_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function bcd_facilities_node_info() {
  $items = array(
    'conference_center' => array(
      'name' => t('National Facility'),
      'base' => 'node_content',
      'description' => t('A national or regional facility.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);// remove after http://drupal.org/node/1949680
  return $items;
}
