<?php
/**
 * @file
 * bcd_communities.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bcd_communities_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "openlayers" && $api == "openlayers_maps") {
    return array("version" => "1");
  }
  if ($module == "openlayers" && $api == "openlayers_styles") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function bcd_communities_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function bcd_communities_node_info() {
  $items = array(
    'center' => array(
      'name' => t('Local Facility'),
      'base' => 'node_content',
      'description' => t('A local facility.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'loc' => array(
      'name' => t('Community'),
      'base' => 'node_content',
      'description' => t('A single community.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'state' => array(
      'name' => t('State'),
      'base' => 'node_content',
      'description' => t('A listing of communities in a state or province.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
