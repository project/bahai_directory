<?php
/**
 * @file
 * bcd_communities.openlayers_styles.inc
 */

/**
 * Implements hook_openlayers_styles().
 */
function bcd_communities_openlayers_styles() {
  $export = array();

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'bcd_temple';
  $openlayers_styles->title = 'Temple';
  $openlayers_styles->description = 'Image of the Baha\'i Temple in Wilmette';
  $openlayers_styles->data = array(
    'externalGraphic' => 'sites/all/modules/bcd/bcd_temple/Wilmette-small.png',
    'pointRadius' => 25,
    'fillColor' => '#EE9900',
    'strokeColor' => '#EE9900',
    'strokeWidth' => 1,
    'fillOpacity' => 0,
    'strokeOpacity' => 0,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicWidth' => 50,
    'graphicHeight' => 54,
    'graphicOpacity' => 1,
    'graphicXOffset' => -25,
    'graphicYOffset' => -45,
    'labelAlign' => 'cm',
  );
  $export['bcd_temple'] = $openlayers_styles;

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'circle';
  $openlayers_styles->title = 'Small circle';
  $openlayers_styles->description = 'Custom style for localities';
  $openlayers_styles->data = array(
    'pointRadius' => 6,
    'fillColor' => '#ff4911',
    'strokeColor' => '#000000',
    'strokeWidth' => 2,
    'fillOpacity' => 1,
    'strokeOpacity' => 0.59999999999999998,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'labelAlign' => 'cm',
  );
  $export['circle'] = $openlayers_styles;

  return $export;
}
