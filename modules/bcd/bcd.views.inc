<?php
// $Id$

/**
 * @file
 *
 */

function bcd_views_pre_render(&$view) {
  $results = &drupal_static(__FUNCTION__);
  if (!isset($results)) {
    $results = 0;
  }
  if ($view->name == 'nearby_map') {
    if ($view->current_display == 'map_block') {
      $info = smart_ip_get_location(ip_address());
      $country = variable_get('bcd_country', FALSE);
      $text = FALSE;
      if (empty($info['longitude']) && empty($info['latitude'])) {
        $text = t('Your location could not be determined. Please choose a state to find communities.');
      }
      elseif (!empty($country) && $info['country_code'] != $country) {
        $text = t('Your location is outside of the country this site serves. Please choose a state to find communities.');
      }
      elseif (empty($results)) {
        $text = t('There were no communities located near your location. Please choose a state to find communities.');
      }
      drupal_alter('bcd_nearby_map_empty_text', $text, $results, $view);
      if (!empty($text)) {
        $view->footer['area_text_custom']->options['content'] = $text;
        $view->footer['area_text_custom']->options['empty'] = FALSE;
      }
      drupal_add_js('(function($){$(document).ready(function(){$("#block-views-nearby_map-map_block-ajax-content").addClass("js-enabled");});})(jQuery);', array('type' => 'inline', 'scope' => 'footer'));
    }
    else {
      $results += count($view->result);
    }
  }
}

function bcd_views_pre_view(&$view, &$display_id, &$args) {
  if ($view->name == 'nearby_map') {
    $info = smart_ip_get_location(ip_address());
    if (!empty($info['latitude']) || !empty($info['longitude'])) {
      $view->display_handler->options['filters']['distance']['value']['latitude'] = $info['latitude'];
      $view->display_handler->options['filters']['distance']['value']['longitude'] = $info['longitude'];
    }
  }
}
