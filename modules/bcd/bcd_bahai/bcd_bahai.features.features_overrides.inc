<?php
/**
 * @file
 * bcd_bahai.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function bcd_bahai_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: field_instance
  $overrides["field_instance.node-loc-field_centers.label"] = 'Local Bahá\'í centers';

  // Exported overrides for: node
//  $overrides["node.center.description"] = 'A Bahá\'í Center serving a local area.';
//  $overrides["node.center.name"] = 'Local Bahá\'í Center';
//  $overrides["node.conference_center.description"] = 'A conference center serving the entire country or a large region.';
//  $overrides["node.conference_center.name"] = 'Conference Center';
//  $overrides["node.loc.description"] = 'A single Bahá\'í community.';
//  $overrides["node.loc.name"] = 'Bahá\'í Community';

 return $overrides;
}
