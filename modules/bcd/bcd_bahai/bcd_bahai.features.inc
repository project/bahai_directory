<?php
/**
 * @file
 * bcd_bahai.features.inc
 */

/**
 * Implements hook_field_default_field_instances_alter().
 */
function bcd_bahai_field_default_field_instances_alter(&$data) {
  if (isset($data['node-loc-field_centers'])) {
    $data['node-loc-field_centers']['label'] = 'Local Bahá\'í centers'; /* WAS: 'Local facilities' */
  }
}

/**
 * Implements hook_node_info_alter().
 */
function bcd_bahai_node_info_alter(&$data) {
  if (isset($data['center'])) {
    $data['center']['description'] = 'A Bahá\'í Center serving a local area.'; /* WAS: 'A local facility.' */
    $data['center']['name'] = 'Local Bahá\'í Center'; /* WAS: 'Local Facility' */
  }
  if (isset($data['conference_center'])) {
    $data['conference_center']['description'] = 'A conference center serving the entire country or a large region.'; /* WAS: 'A national or regional facility.' */
    $data['conference_center']['name'] = 'Conference Center'; /* WAS: 'National Facility' */
  }
  if (isset($data['loc'])) {
    $data['loc']['description'] = 'A single Bahá\'í community.'; /* WAS: 'A single community.' */
    $data['loc']['name'] = 'Bahá\'í Community'; /* WAS: 'Community' */
  }
}
